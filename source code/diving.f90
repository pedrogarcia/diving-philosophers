! diving.f90
! The Divers Problem
!
! Authot: Pedro Garcia Freitas
! October, 30. 2009
!
!
module heaper
  use omp_lib
  implicit none

  integer, parameter :: N = 5 ! number of divers
  integer, parameter :: IDLE = 0 ! if diver is idle
  integer, parameter :: BREATHLESS = 1 ! if the diver must replace oxygen
  integer, parameter :: WORKING = 2 ! state when diver is using 2 oxygen resources

  integer, dimension(0:N-1) :: s = 0, state = 0
  integer :: mutex = 1

  contains

  integer function LEFT(i)
    integer, intent(in) :: i
    LEFT = mod(i+N-1,N)
  end function LEFT

  integer function RIGHT(i)
    integer, intent(in) :: i
    RIGHT = mod(i+1,N)    
  end function RIGHT

  integer function my_rand()
    real :: r(1)
    INTEGER :: i, n, clock
    INTEGER, DIMENSION(:), ALLOCATABLE :: seed
      
    call random_seed(size = n)
    allocate(seed(n))      
    call system_clock(COUNT=clock)      
    seed = clock + 37 * (/ (i - 1, i = 1, n) /)
    call random_seed(PUT = seed)      
    deallocate(seed)
    call random_number(r)
    my_rand = int(r(1)*3) + 1
  end function my_rand

  subroutine down(i)
    integer, intent(inout) :: i
    !$omp atomic
    i = i-1
  end subroutine down

  subroutine up(i)
    integer, intent(inout) :: i
    !$omp atomic
    i = i + 1
  end subroutine up

  subroutine diver(i)
    integer, intent(in) :: i

    do
      call idle_(i)
      call take_oxygen(i)
      call work(i)
      call put_oxygen(i)
    end do
  end subroutine diver

  subroutine idle_(i)
    integer, intent(in) :: i
    !$OMP CRITICAL
    call showStatus()
    !$OMP END CRITICAL
    call sleep(my_rand())
  end subroutine idle_

  subroutine work(i)
    integer, intent(in) :: i
    !$OMP CRITICAL
    call showStatus()
    !$OMP END CRITICAL
    call sleep(my_rand())
  end subroutine work

  subroutine take_oxygen(i)
    integer, intent(in) :: i
    
    call down(mutex)
    state(i) = BREATHLESS
    call test(i)
    call up(mutex)
    call down(s(i))
  end subroutine take_oxygen

  subroutine put_oxygen(i)
    integer, intent(in) :: i
    
    call down(mutex)
    state(i) = IDLE
    call test(LEFT(i))
    call test(RIGHT(i))
    call up(mutex)
  end subroutine put_oxygen

  subroutine test(i)
    integer, intent(in) :: i

    if((state(i) == BREATHLESS) .and. (state(LEFT(i)) /= WORKING) &
      & .and. (state(RIGHT(i)) /= WORKING))then

      state(i) = WORKING
      call up(s(i))
    end if
  end subroutine test

  subroutine showStatus()  
    if(OMP_GET_THREAD_NUM() >= 0)then
      call system("clear")
      print *
      print *," Segundo Trabalho de NSO "
      print *,""
      print *,"Alunos: Pedro Garcia Freitas"
      print *,"        Felipe Gomes Lacerda"
      print *
      print *
      print *
      print *
      print *, masked(state)
    end if

    contains 
      function mask(code)
        integer, intent(in) :: code
        character(len=15) :: mask

        select case(code)
          case(0)
            mask = "    IDLE     "
          case(1)
            mask = "  BREATHLESS "
          case(2)
            mask = "   WORKING   "
        end select
      end function mask

      function masked(input)
        integer :: i
        integer, dimension(N) :: input
        character(len=15), dimension(N) :: masked

        do i = 1, N
          masked(i) = mask(input(i))
        end do
      end function masked  
  end subroutine showStatus

  subroutine init()
    integer :: process_id
  
    call omp_set_dynamic(.false.)
    call omp_set_num_threads(N)

    !$OMP PARALLEL SHARED (s,state,mutex) PRIVATE(process_id)
      process_id = OMP_GET_THREAD_NUM()
      call diver(process_id)
    !$OMP END PARALLEL
  end subroutine init
end module heaper

program diving
  use heaper
  call init()
end program diving
