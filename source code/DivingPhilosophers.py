# -*- coding: utf-8 -*-
#
# DivingPhilosophers.py
#
# Sincroniza 5 mergulhadores que precisam compartilhar 5 balões de
# oxigênio entre eles.
#
# Autor: Pedro Garcia Freitas <sawp@sawp.com.br>
# Baseado no algoritmo de Dijkstra para resolução do problema do
# jantar dos filósofos
#
# Licença: Domínio Público
#

from threading import Thread
from threading import Lock
from random import randint
import time
import sys
import signal


TIME1 = 2
TIME2 = 4
N = 5  # number of divers

IDLE = 0  # if diver is idle
BREATHLESS = 1  # if the diver must replace oxygen
WORKING = 2  # state when diver is using 2 oxygen resources

screen_row = [10, 6, 6, 10, 14]
screen_col = [31, 36, 44, 49, 40]

LEFT = lambda i: (i + N - 1) % N  # code of ith left neighbor
RIGHT = lambda i: (i + 1) % N  # code of ith right neighbor
up = lambda a: a.release()
down = lambda a: a.acquire()


class Global:
    # semaphore to indicate mutual exclusion for critical regions
    mutex = Lock()
    # initialize the semaphore set of divers
    s = [Lock()] * N
    # diver's states (0 idle, 1 breathless, 2 working)
    state = [BREATHLESS] * N


class Diver(Thread):
        """ One class for Thread (Thread for class) """
        def __init__(self, i):
            Thread.__init__(self)
            self._i = i

        def run(self):
            self._diver(self._i)

        def _diver(self, i):
            """ Activities of ith diver (from 0 to N-1) """
            while True:
                idle(i)
                take_oxygen(i)
                work(i)
                put_oxygen(i)


def showStatus():
    def position(row, col):
        s_exit = "\033[%d;%dH" % (row, col)
        sys.stdout.write(s_exit)

    def position_flush(row, col):
        s_exit = "\033[%d;%dH\n" % (row, col)
        sys.stdout.write(s_exit)

    def draw_breathless(n):
        position(screen_row[n], screen_col[n])
        sys.stdout.write("\033[34mB\033[0m")
        position_flush(13, 1)

    def draw_idle(n):
        position(screen_row[n], screen_col[n])
        sys.stdout.write("\033[33mI\033[0m")
        position_flush(13, 1)

    def draw_working(n):
        position(screen_row[n], screen_col[n])
        sys.stdout.write("\033[31mW\033[0m")
        position_flush(13, 1)

    def draw(n):
        for i in range(0, N):
            code = Global.state[i]
            if code == IDLE:
                draw_idle(i)
            elif code == BREATHLESS:
                draw_breathless(i)
            else:
                draw_working(i)

    def mask(code):
        if code == IDLE:
            return "   IDLE   "
        elif code == BREATHLESS:
            return "BREATHLESS"
        else:
            return " WORKING  "

    showStatus.i += 1

    try:
        print '\x1b[H\x1b[2J'
        print " Segundo Trabalho de NSO "
        print ""
        print "Alunos: Pedro Garcia Freitas"
        print "        Felipe Gomes Lacerda"
        draw(i)
        print "\n\n\n\n"
        print str(map(mask, Global.state))
    finally:
        pass

showStatus.i = 0


def idle(i):
    """ indicates that there is no supply of oxygen """
    showStatus()
    time.sleep(randint(TIME1, TIME2))


def work(i):
    """ work when have two oxygen supplies """
    showStatus()
    time.sleep(randint(TIME1, TIME2))


def take_oxygen(i):
    """ acquire two oxygens to work """
    down(Global.mutex)        # start critical work (enter critical region)
    Global.state[i] = BREATHLESS  # HEY, i need breath too
    test(i)                       # try work with 2 oxygen suplies
    up(Global.mutex)              # exit critical region
    if not Global.s[i].locked():
        down(Global.s[i])     # block if oxygen suply not acquired


def put_oxygen(i):
        """  free oxygen supply """
        down(Global.mutex)         # enter critical region
        Global.state[i] = IDLE     # diver has finished the work
        test(LEFT(i))              # see if left neighbor needs restore oxygen
        test(RIGHT(i))             # see if right neighbor needs restore oxygen
        up(Global.mutex)           # free the critical region


def test(i):
    """  Tests which diver needs to restore oxygen """
    if (Global.state[i] == BREATHLESS) and \
       (Global.state[LEFT(i)] != WORKING) and \
       (Global.state[RIGHT(i)] != WORKING):
        Global.state[i] = WORKING
        if Global.s[i].locked():
            up(Global.s[i])


def signal_listener():
    for sig in range(1, signal.NSIG):
        try:
            signal.signal(sig, signal.SIG_DFL)
        except RuntimeError:
            pass


if __name__ == "__main__":
    print '\x1b[H\x1b[2J'
    for i in range(0, N):
        Diver(i).start()
        signal_listener()
