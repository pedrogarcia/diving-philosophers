{-
   DivingPhilosophers.hs

   Sincroniza 5 mergulhadores que precisam compartilhar 5 balões de
   oxigênio entre eles.

   Autor: Felipe Gomes Lacerda <fegolac@gmail.com>

   Compilar com ghc -o diving diving.hs --make -threaded
-}

module Main where

import Random
import Control.Monad
import Control.Concurrent
import Control.Concurrent.STM
import Data.Array

-- Balões implementados como semáforos binários usando STM
type Bottle = TVar Bool

newBottle :: IO Bottle
newBottle = newTVarIO True

get :: Bottle -> STM ()
get bottle = do b <- readTVar bottle
                -- se b não estiver livre, reinicia a transação
                check b
                writeTVar bottle False

free :: Bottle -> STM ()
free bottle = writeTVar bottle True

type Buffer = TVar (Array Int String)

newBuffer :: Int -> IO Buffer
newBuffer n =
    let init_list = [(i,"") | i <- [0..n-1]] in
    newTVarIO (array (0,n-1) init_list)

writeState buffer item n = do arr <- readTVar buffer
                              let a = arr // [(n,item)]
                              writeTVar buffer a

readState buffer n = do arr <- readTVar buffer
                        return (arr ! n)

output buffer n m
    | n > m = do putStr "\n"
                 threadDelay 1000000
                 output buffer 0 m
    | otherwise = do str <- atomically (readState buffer n)
                     putStr str
                     output buffer (n+1) m

diver :: Int -> Buffer -> Bottle -> Bottle -> IO ()
diver i buffer bottle1 bottle2 =
    -- começa sem respirar
    do atomically (writeState buffer (show i ++ " sem respirar\t") i)

       -- tenta adquirir os dois balões
       atomically (do
         get bottle1
         get bottle2)

       -- respira por um tempo aleatório
       atomically (writeState buffer (show i ++ " respirando\t") i)
       randomDelay

       -- libera os balões
       atomically (do
         free bottle1
         free bottle2)

       -- tempo 'ocioso' (o mergulhador não precisa de oxigênio)
       atomically (writeState buffer (show i ++ " ocioso\t") i)
       randomDelay

       -- repete o procedimento
       diver i buffer bottle1 bottle2
    where
      randomDelay = do r <- randomRIO(100000,500000)
                       threadDelay r

simulate :: Int -> IO ()
simulate n = do bottles <- replicateM n newBottle
                outputBuffer <- newBuffer n
                mapM_ (action outputBuffer bottles) [0..n-1]
                output outputBuffer n (n-1)
    where
      action buffer bottles i =
          forkIO (diver i buffer
                  (bottles !! i)
                  (bottles !! ((i+1) `mod` n)))

main = simulate 5
