# Diving Philosophers

Paper and code for a college work. This work consists in a custom version of 
Dining philosophers problem. The problem is described as follows: "Five
divers are submerged under depth of 220 meters. To do some work and breathe
at the same time, they must balance the internal pressure of their costume
with the external water pressure. Therefore, they need to use 2 oxygen bottles:
one for breathing and another for pressure control. However, for each meter 
submerged, the weight of a body is increased by 10 times, when compared to the
surface weight. Thus, each diver can submerge with a unique oxygen bottle. So,
how can they share their oxygen bottles? Each of them needs to breathe and
work. The clothes store oxygen enough to the divers breathe, since they are
not working underwater."


Implementation in Haskell, Fortran, and Python.