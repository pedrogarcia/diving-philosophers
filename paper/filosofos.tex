\documentclass[10pt,a4paper,twocolumn]{article}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc}
\usepackage{mathpazo}
\usepackage{appendix}
\usepackage{url}
\usepackage{a4wide}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fancyvrb}
\usepackage{graphicx}
\usepackage[margin=1.5cm]{geometry}
\author{Felipe Gomes Lacerda \\ 06/83884 \and Pedro Garcia Freitas \\ 04/35597}
%\date{23 de novembro de 2009}
\title{Problema dos mergulhadores}
\makeatletter
\g@addto@macro\@verbatim\scriptsize
\makeatother
\DefineVerbatimEnvironment{snippet}{Verbatim}{fontfamily=tt,numbers=left,
  fontsize=\scriptsize,
  xleftmargin=3mm,numbersep=3pt}
\providecommand{\appendixname}{Apêndice}

\begin{document}
\maketitle
\VerbatimFootnotes

\section{O problema}

5 mergulhadores estão submersos a uma profundidade de 220 metros. Para eles
conseguirem respirar a esta profundidade e realizar algum trabalho, precisam
equilibrar a pressão interna do seu traje com a pressão externa da água. Para
haver esse equilíbrio, precisam utilizar 2 balões de oxigênio: um para
respiração e outro para controle de pressão. Contudo, cada metro submerso
equivale a aumentar em 10 vezes o peso de um corpo em relação à pressão
atmosférica. Sendo assim, cada mergulhador só pode submergir com 1 balão de
oxigênio. Como possibilitar que eles compartilhem seus balões de oxigênio para
que possam trabalhar, sendo que a roupa armazena e suporta oxigênio suficiente
por um período de tempo, desde que os mergulhadores não realizem trabalho?

\section{Solução 1: STM}

A primeira solução ao problema foi feita na linguagem Haskell, usando
\textit{Software Transactional Memory}~\cite{stm} (STM). STM é um novo modelo
para programação concorrente, cuja filosofia é baseada em transações
bancárias. Em particular, esse modelo tem uma visão \textit{otimista} de seções
críticas: várias threads podem executar uma seção crítica ao mesmo tempo. Mas em
vez de as escritas serem feitas nas variáveis compartilhadas diretamente, cada
thread escreve em um log as mudanças a serem feitas. Quando a seção crítica
(também chamada de \textit{transação}) termina, o log é validado: o sistema
verifica se os valores das variáveis lidas---os valores são armazenados no
log---são iguais aos verdadeiros valores das variáveis. Se forem iguais, as
alterações do log são gravadas nas variáveis. Se forem diferentes, é porque
outras threads alteraram as variáveis---a transação tinha uma visão incosistente
da memória. Nesse caso, o log e a transação são reiniciados.

Esse modelo tem algumas vantagens em relação ao usual, que usa mutexes---por
exemplo, nunca precisamos nos preocupar em tomar o número certo de mutexes, ou o
local e a ordem em que eles são colocados. Só temos que nos preocupar em como
delimitar as transações. Isso elimina uma grande classe de possíveis
deadlocks. Ainda, esse modelo fornece mais concorrência que mutexes, já que
várias threads podem executar uma transação ao mesmo tempo, desde que a
consistência seja garantida.

O problema com o modelo é que ele é difícil de implementar nas linguagens mais
populares. Variáveis usadas em transações, também chamadas de variáveis
transacionais, devem ser manipuladas de forma diferente das não-transacionais, e
a linguagem deve impedir que uma variável transacional seja modificada fora de
uma transação. Ainda, ela deve controlar as operações que ocorrem dentro de uma
transação---por exemplo, operações de entrada e saída não podem ser
revertidas. É difícil implementar essas restrições em linguagens como Java e
C++, mas elas são viáveis em Haskell. Previsivelmente, Haskell foi a primeira
linguagem a implementar STM.

Como, então, usamos STM para resolver o problema? Primeiro, vamos modelar os
balões de oxigênio. Um balão será representado por um semáforo binário, que
assume os valores \verb|False| (que indica que o balão está sendo usado) e
\verb|True| (balão livre).

\begin{snippet}
type Bottle = TVar Bool
\end{snippet}

Essa declaração indica que um balão é uma variável transacional (\verb|TVar|) do
tipo booleano.

\begin{snippet}
newBottle :: IO Bottle
newBottle = newTVarIO True

get :: Bottle -> STM ()
get bottle = do b <- readTVar bottle
                -- se b não estiver livre, reinicia a transação
                check b
                writeTVar bottle False

free :: Bottle -> STM ()
free bottle = writeTVar bottle True
\end{snippet}

A função \verb|newBottle| cria um balão, inicialmente livre. A função
\verb|take| tenta adquirir um balão. Se ele já foi tomado, \textit{toda a
  transação é reiniciada}. \verb|free| simplesmente libera o balão.

Um problema pode ocorrer se a thread reiniciar a transação imediatamente após
verificar que o balão está tomado. Se ela fizer isso, é bem provável que o balão
ainda esteja tomado na segunda tentativa. Em geral, várias tentativas são
necessárias para conseguir o balão. Assim, muito tempo de processamento pode ser
perdido nessas novas tentativas. Mas como as threads mantêm logs de variáveis
utilizadas durante a transação, uma otimização pode ser feita: a thread só
reinicia a transação quando uma das variáveis for modificada.

Antes de partir para a solução do problema, falta mais um detalhe. Como apontado
anteriormente, operações de entrada e saída não podem ser revertidas, então não
faz sentido fazer I/O dentro de uma transação. Como, então, imprimimos o estado
de cada mergulhador? Uma forma é escrever o estado em uma variável transacional
(um buffer) e então imprimir esse buffer fora da transação.

\begin{snippet}
type Buffer = TVar (Array Int String)

newBuffer :: Int -> IO Buffer
newBuffer n =
    let init_list = [(i,(show i)++"") | i <- [0..n-1]] in
    newTVarIO (array (0,n-1) init_list)

writeState buffer item n = do arr <- readTVar buffer
                              let a = arr // [(n,item)]
                              writeTVar buffer a

readState buffer n = do arr <- readTVar buffer
                        return (arr ! n)

output buffer n m
    | n > m = do putStr "\n"
                 threadDelay 500000
                 output buffer 0 m
    | otherwise = do str <- atomically (readState buffer n)
                     putStr str
                     output buffer (n+1) m
\end{snippet}

\verb|newBuffer| cria uma array para armazenar o estado de cada mergulhador. A
array em si é uma variável transacional, para permitir a atualização dos estados
durante uma transação. \verb|writeState| substitui a array por outra que tem o
estado do \verb|n|-ésimo mergulhador atualizado. \verb|readState| lê o estado do
\verb|n|-ésimo mergulhador (isso é necessário para imprimir os
estados). \verb|output| é um loop infinito: a cada meio segundo, a função
imprime o estado de todos os mergulhadores. Os estados em geral serão diferentes
a cada iteração, porque cada mergulhador vai modificar o buffer com o seu estado
atual.

Finalmente, a função que representa um mergulhador.

\begin{snippet}
diver :: Int -> Buffer -> Bottle -> Bottle -> IO ()
diver i buffer bottle1 bottle2 =
    -- começa sem respirar
    do atomically (writeState buffer (show i ++ " sem respirar\t") i)

       -- tenta adquirir os dois balões
       atomically (do
         get bottle1
         get bottle2)

       -- respira por um tempo aleatório
       atomically (writeState buffer (show i ++ " respirando\t") i)
       randomDelay

       -- libera os balões
       atomically (do
         free bottle1
         free bottle2)

       -- tempo 'ocioso' (o mergulhador não precisa de oxigênio)
       atomically (writeState buffer (show i ++ " ocioso\t") i)
       randomDelay

       -- repete o procedimento
       diver i buffer bottle1 bottle2
    where
      randomDelay = do r <- randomRIO(100000,500000)
                       threadDelay r
\end{snippet}

O mergulhador tenta tomar dois balões. Essa operação deve ser colocada em uma
transação, ou então poderia acontecer o problema de todos os mergulhadores
pegarem um balão e então entrarem em deadlock porque não há mais nenhum balão
livre. Uma transação é delimitada pela função \verb|atomically|. Note que a
escrita dos estados e a liberação dos balões também estão em transações.

Por fim, a função para simular o problema para \verb|n| mergulhadores.

\begin{snippet}
simulate :: Int -> IO ()
simulate n = do bottles <- replicateM n newBottle
                outputBuffer <- newBuffer n
                mapM_ (action outputBuffer bottles) [0..n-1]
                output outputBuffer n (n-1)
    where
      action buffer bottles i =
          forkIO (diver i buffer
                  (bottles !! i)
                  (bottles !! ((i+1) `mod` n)))
\end{snippet}

Essa função cuida da criação dos balões, do buffer e dos mergulhadores em si,
sendo que cada um executa numa thread diferente. Para criar threads, a função
\verb|forkIO| é usada. Cada mergulhador então toma o balão ``da esquerda'' e o
``da direita''. Note que a última chamada do código é para a função
\verb|output|, que é um loop infinito que imprime o estado de cada mergulhador a
cada 0.5 segundos.

\section{Solução 2: Mutex}

A segunda solução do problema utiliza um mutex para sincronizar o acesso das
threads à região crítica, além de manter um conjunto de semáforos para indicar o
estado dos recursos.

No programa, cada mergulhador é um ente independente, implementado na seguinte
classe:

\begin{snippet}
 class Diver(Thread):
        def __init__(self,i):
                Thread.__init__(self)
                self._i = i

        def run(self):
                self._diver(self._i)

        def _diver(self, i):
                """ Activities of ith diver (from 0 to N-1) """

                while True:
                        idle(i)
                        take_oxygen(i)
                        work(i)
                        put_oxygen(i)
\end{snippet}

No código acima, o construtor da classe \verb|__init__| serve para criar uma
thread nova no sistema. O argumento \verb|i| passado ao construtor será uma
variável inteira, que é a identificação da thread, representando o $i$-ésimo
mergulhador.

O método \verb|run| é utilizado pelo sistema para reconhecer qual serão as
instruções que aquela thread deverá executar, ou seja, onde é implementada sua
atividade. Note que este método chama a função privada \verb|_diver|, utilizada
para conter as atividades de cada mergulhador---equivalentes a cada função
chamada em seu corpo (\verb|idle|, \verb|take_oxygen|, \verb|work| e
\verb|put_oxygen|).

A primeira das funções chamadas, \verb|idle|, apenas faz com que o mergulhador
aguarde um tempo aleatório até tentar obter um recurso (balão de oxigênio):

\begin{snippet}
def idle(i):
        showStatus()
        time.sleep( randint(TIME1,TIME2) )
\end{snippet}

A segunda linha do código acima contém a função \verb|showStatus|, que apenas
imprime de forma amigável para o usuário os estados de cada mergulhador. A
terceira linha que contém a verdadeira funcionalidade de \verb|idle|, que é
colocar a thread para dormir por um tempo aleatório, definido no intervalo entre
\verb|TIME1| e \verb|TIME2| (no código, definimos \verb|TIME1| e \verb|TIME2|
como 2 e 4, respectivamente).

Passado o tempo em que a thread dorme, a próxima função que ela executará será
\verb|take_oxygen|. Conforme sugere o nome, ao executar esta função a thread que
representa o mergulhador tentará tomar posse dos recursos envolvidos no
compartilhamento, que estão encapsulados na seguinte classe:

\begin{snippet}
class Global:
        mutex = Lock()
        s = [Lock()] * N
        state = [BREATHLESS] * N
\end{snippet}

Os objetos do tipo \verb|Lock| são a forma que o Python fornece mutexes para
sincronização das threads, sendo que eles podem assumir dois estados: bloqueados
ou não-bloqueados. É a partir destes estados que a função \verb|take_oxygen| irá
decidir se deve obter o recurso ou não.

O atributo \verb|s| é uma lista, onde cada elemento $i$ equivale ao estado da
$i$-ésima thread. Desta forma, antes de um mergulhador adquirir um novo balão de
oxigênio, ele saberá se possui todos os recursos que precisa para começar a
execução de seu trabalho.

A terceira variável da classe \verb|Global| é uma lista que representa os
possíveis estados do mergulhador---trabalhando, aguardando ou necessitando
repor oxigênio. O mergulhador começa precisando repor oxigênio
(\verb|BREATHLESS|), mas ele também pode assumir os estados \verb|WORKING| e
\verb|IDLE|.

Portanto, a função \verb|take_oxygen| terá a seguinte forma:

\begin{snippet}
def take_oxygen(i):
        down(Global.mutex)
        Global.state[i] = BREATHLESS
        test(i)
        up(Global.mutex)
        if not Global.s[i].locked():
                down(Global.s[i])
\end{snippet}

A segunda linha desta função serve para indicar que a thread entrou na
região crítica. Como esta operação é atômica, certamente outra thread
não irá fazer o mesmo, garantindo assim que a atual poderá realizar as operações
das linhas 3 e 4 sem que o estado das outras mude, não havendo condição de
corrida.

Sendo assim, a thread atual muda seu estado, indicando sua intenção de
obter o recurso. Para os mergulhadores, isto equivale ao fato que todos eles
querem repor seu oxigênio o quanto antes, conforme pode ser visto na linha 3.

Ainda de posse da região crítica, na linha 4 o mergulhador testa para ver se
existem os 2 balões de oxigênio necessários para que ele possa trabalhar e
respirar. A função desta linha simplesmente verifica se os dois mergulhadores
mais próximos não estão trabalhando, ou seja, se os balões de oxigênio
compartilhados estão livres. Caso estejam, a função automaticamente os libera
para que o mergulhador trabalhe e reponha sua respiração.

A linha 5, quando executa \verb|up(Global.mutex)|, libera a região crítica,
fazendo com que a thread atual não bloqueie mais o estado das demais.

Nas duas últimas linhas a thread verifica que o recurso não está sendo usado. Se
ele não estiver, então ela o adquire para si.

Note que a execução da thread atual só continua após o término da função
\verb|test|. Ou seja, a função \verb|work| apenas executa após o mergulhador
estar de posse dos 2 recursos necessários. No código, esta função não realiza
nenhuma tarefa real, sendo utilizada apenas para representar o período de tempo
em que o mergulhador estivesse trabalhando.

Por fim, a última função chamada pela classe \verb|Diver| é a
\verb|put_oxygen|. Esta função apenas libera os recursos alocados por
\verb|take_oxygen|:

\begin{snippet}
def put_oxygen(i):
        down(Global.mutex)
        Global.state[i] = IDLE
        test(LEFT(i))
        test(RIGHT(i))
        up(Global.mutex)
\end{snippet}

Conforme explicado para \verb|take_oxygen|, as linhas 2 e 6 da função acima
servem para garantir e liberar o acesso da thread atual na região crítica. A
terceira linha indica que o mergulhador está ocioso por não estar com os balões
necessários e as 2 linhas seguintes servem para transferir o recurso para os
mergulhadores vizinhos.

\section{Conclusão}

STM é um mecanismo de controle de concorrência muito mais simples
conceitualmente que a sincronização baseada em mutexes. Ele permite que o
programador tenha um modelo mais claro de como as threads do programa interagem;
em particular, deadlocks são muito menos frequentes. No caso do problema dos
mergulhadores, o deadlock é evitado simplesmente colocando a operação de pegar
os dois balões numa transação.

Por outro lado, STM é difícil de implementar em linguagens populares devido à
natureza das transações: como elas devem ser revertidas frequentemente, não
podemos executar operações que não podem ser desfeitas, como a maioria das
operações de I/O. No problema dos mergulhadores, a solução para a questão de
imprimir o estado na tela foi resolvido colocando os estados num buffer e depois
imprimindo-os fora de uma transação.

A outra solução, baseada em mutexes, foi feita para demonstrar a solução do
problema dos mergulhadores em um nível de abstração mais baixo. Desta forma,
podemos simular o funcionamento de como seria a comunicação entre os elementos
de hardware, se a distribuição dos recursos fosse controlada por um computador,
por exemplo.

\begin{thebibliography}{widest-label}
\bibitem{stm}
  \url{http://research.microsoft.com/en-us/um/people/simonpj/papers/stm/beautiful.pdf}
\end{thebibliography}

\appendix
\newpage

\renewcommand\thesection{Apêndice \Alph{section}}
\onecolumn

\section{Implementação utilizando STM}
\fvset{fontsize=\normalsize}

\VerbatimInput{diving.hs}

\newpage

\section{Implementação utilizando Python}

\VerbatimInput{DivingPhilosophers.py}

\end{document}
